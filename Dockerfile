FROM openjdk:8
COPY target/stats-application.jar /app.jar
CMD java -jar /app.jar