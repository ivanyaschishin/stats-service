# Upload Stats Application
### REST API Endpoints:
- `GET /statistics`: returns 200 OK HTTP Response and current upload statistics
- `POST /upload`: registers upload event by updating its upload statistics, return `201 CREATED` if successful or `204 NO_CONTENT` HTTP responses. Request body should contain JSON object with fields `count` - integer number of uploads and `timestamp` - unix epoch timestamp of evvent's occurrence time. 

### Building and Running
To build JAR-file itself execute `mvn clean install` in root directory of project.
To build and execute application inside of local docker run `./start.sh` - this will build application itself and local
docker image and create and start local docker container with the application and expose it on `localhost:8080`.
To stop application run `./stop.sh`.

### License
This application is licensed under MIT license.

### FAQ
**Why does it use one thread to execute all updates?**

Because its impossible to maintain consistency across several threads on multiple
values without ever failing a write and possibly forcing writer to do multiple 
attempts. For this domain it would be unacceptable to do so. For example if this service
allowed concurrent writes on different counters then at least average and max, min counters 
would become inconsistent with sum and count counters. Again approach with failing writes, which is 
basically using atomic CAS and TAS operations would lead to writer attempting to write
new update several times because another writer would update something before it. All in all
if this was just one counter or counters were not related, a simple increment on a volatile value would suffice.

**Why does it use PriorityQueue for executing updates (and why did it use this ugly solution with while(trues) and pair of volatile booleans):**
This is done to guarantee that counters are reset every N seconds. In highly concurrent environment
the execution queue would be constantly filled with UpdateTasks and ResetTask would be executed
too late. First solution used a pair of non-blocking "locks", current solution uses prioritization
in Queue. It is guaranteed that ResetTask will be executed either right after its added to queue or after 
currently running update is executed. 