package com.isc.stats.controller;

import com.isc.stats.dto.UploadEventDTO;
import com.isc.stats.dto.UploadStatisticsDTO;
import com.isc.stats.exception.TimestampExpiredException;
import com.isc.stats.service.CounterService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
public class StatisticsController {
    private final Logger logger = LoggerFactory.getLogger(StatisticsController.class);
    private final CounterService counterService;

    public StatisticsController(CounterService counterService) {
        this.counterService = counterService;
    }

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void registerEvent(@RequestBody UploadEventDTO eventDTO) {
        if(!counterService.registerEvent(eventDTO)) {
            throw new TimestampExpiredException(eventDTO.getTimestamp());
        }
    }

    @RequestMapping(value = "/statistics", method = RequestMethod.GET)
    public UploadStatisticsDTO getStatistics() {
        return counterService.getReport();
    }

    @ExceptionHandler(TimestampExpiredException.class)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void timestampExpiredExceptionHandler(TimestampExpiredException exception) {
        logger.warn("Failed to register event: ", exception);
    }
}
