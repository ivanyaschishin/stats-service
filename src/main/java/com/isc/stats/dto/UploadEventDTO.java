package com.isc.stats.dto;

public class UploadEventDTO {
    private long count;
    private long timestamp;

    public UploadEventDTO() {
    }

    public UploadEventDTO(long count, long timestamp) {
        this.count = count;
        this.timestamp = timestamp;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }
}
