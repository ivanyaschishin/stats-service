package com.isc.stats.exception;

public class TimestampExpiredException extends RuntimeException {
    public TimestampExpiredException(long timestampValue) {
        super("Timestamp " + timestampValue + " is too old.");
    }
}