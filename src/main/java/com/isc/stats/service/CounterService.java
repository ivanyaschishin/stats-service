package com.isc.stats.service;

import com.isc.stats.dto.UploadEventDTO;
import com.isc.stats.dto.UploadStatisticsDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.Clock;

@Service
public class CounterService {
    private final Logger logger = LoggerFactory.getLogger(CounterService.class);
    private final Clock clock;
    private final Long timeWindowMs;
    private final boolean disableResetThread;
    private final CounterUpdateScheduler updateScheduler;
    private final CounterStorage storage;

    public CounterService(Clock clock,
                          @Value("${stats.time.window.ms:60000}") Long timeWindowMs,
                          @Value("${stats.reset.thread.disabled:false}") Boolean disableResetThread,
                          CounterUpdateScheduler updateScheduler, CounterStorage storage) {
        this.clock = clock;
        this.timeWindowMs = timeWindowMs;
        this.disableResetThread = disableResetThread;
        this.updateScheduler = updateScheduler;
        this.storage = storage;
    }

    @PostConstruct
    public void init() {
        logger.debug("Starting CounterService.");

        if (!disableResetThread) {
            Thread resetThread = new Thread(this::resetLoop);
            resetThread.start();
        }
    }

    private void resetLoop() {
        logger.debug("Started background stats reset thread with time window: {}", timeWindowMs);
        while (true) {

            resetCounters();

            try {
                Thread.sleep(timeWindowMs);
            } catch (InterruptedException ignored) {
                return;
            }
        }
    }

    public void resetCounters() {
        updateScheduler.reset();
    }

    public UploadStatisticsDTO getReport() {
        return storage.getSnapshot();
    }

    public boolean registerEvent(UploadEventDTO event) {
        if (event.getTimestamp() < (clock.millis() - timeWindowMs)) {
            return false;
        }

        updateScheduler.processEvent(event);

        return true;
    }
}
