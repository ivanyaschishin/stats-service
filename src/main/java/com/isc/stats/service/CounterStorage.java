package com.isc.stats.service;

import com.isc.stats.dto.UploadStatisticsDTO;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class CounterStorage {
    private volatile long count = 0L;
    private volatile long sum = 0L;
    private volatile long max = 0L;
    private volatile long min = Long.MAX_VALUE;
    private volatile UploadStatisticsDTO snapshot;

    @PostConstruct
    public void init() {
        snapshot = new UploadStatisticsDTO();
        snapshot.setCount(0L);
        snapshot.setSum(0L);
        snapshot.setMax(0L);
        snapshot.setMin(0L);
        snapshot.setAverage(0.0);
    }

    public synchronized void updateCounters(long uploadCount) {
        count += 1;
        sum += uploadCount;

        min = min > uploadCount ? uploadCount : min;
        max = max < uploadCount ? uploadCount : max;

        snapshot = createSnapshot();
    }

    public synchronized void reset() {
        count = 0L;
        sum = 0L;
        max = 0L;
        min = Long.MAX_VALUE;
        snapshot = createSnapshot();
    }

    public UploadStatisticsDTO getSnapshot() {
        return snapshot;
    }

    private UploadStatisticsDTO createSnapshot() {
        UploadStatisticsDTO snapshot = new UploadStatisticsDTO();
        snapshot.setCount(count);
        snapshot.setSum(sum);
        snapshot.setMin(min == Long.MAX_VALUE ? 0 : min);
        snapshot.setMax(max);
        snapshot.setAverage(count == 0 ? 0.0 : (double) sum / (double) count);


        return snapshot;
    }
}
