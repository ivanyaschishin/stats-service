package com.isc.stats.service;

import com.isc.stats.dto.UploadEventDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Queue;
import java.util.concurrent.PriorityBlockingQueue;

@Service
public class CounterUpdateScheduler {
    private final Logger logger = LoggerFactory.getLogger(CounterUpdateScheduler.class);
    private final CounterStorage storage;
    private Queue<Task> taskQueue = new PriorityBlockingQueue<>(100, new TaskPriorityComparator());
    private volatile boolean isRunning;
    private Thread updateThread;

    public CounterUpdateScheduler(CounterStorage storage) {
        this.storage = storage;
    }

    @PostConstruct
    public void init() {
        start();
    }

    public boolean processEvent(UploadEventDTO event) {
        return taskQueue.offer(new UpdateTask(storage, event));
    }

    public boolean reset() {
        return taskQueue.offer(new ResetTask(storage));
    }

    private void updateLoop() {
        while (isRunning) {
            executeNextTask();
        }
    }

    private boolean executeNextTask() {
        Task task = taskQueue.poll();
        if (task != null) {
            task.run();
            return true;
        }

        return false;
    }

    public void start() {
        isRunning = true;
        updateThread = new Thread(this::updateLoop);
        updateThread.start();
    }

    public void stop() throws InterruptedException {
        isRunning = false;
        updateThread.join();
    }

    public void flushQueue() {
        long counter = 0;
        while (executeNextTask()) {
            counter++;
        }
        logger.info("Executed {} remaining tasks.", counter);
    }
}
