package com.isc.stats.service;

public class ResetTask implements Task {
    private CounterStorage storage;

    public ResetTask(CounterStorage storage) {
        this.storage = storage;
    }

    @Override
    public int getPriority() {
        return Integer.MAX_VALUE;
    }

    @Override
    public void run() {
        storage.reset();
    }
}
