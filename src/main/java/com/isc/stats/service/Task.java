package com.isc.stats.service;

public interface Task extends Runnable {
    int getPriority();
}
