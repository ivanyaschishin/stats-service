package com.isc.stats.service;

import java.util.Comparator;

public class TaskPriorityComparator implements Comparator<Task> {

    @Override
    public int compare(Task left, Task right) {
        return left.getPriority() - right.getPriority();
    }
}
