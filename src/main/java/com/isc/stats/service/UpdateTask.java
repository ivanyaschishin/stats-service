package com.isc.stats.service;

import com.isc.stats.dto.UploadEventDTO;

public class UpdateTask implements Task {
    private UploadEventDTO event;
    private CounterStorage storage;

    public UpdateTask(CounterStorage storage, UploadEventDTO event) {
        this.storage = storage;
        this.event = event;
    }

    @Override
    public void run() {
        storage.updateCounters(event.getCount());
    }

    @Override
    public int getPriority() {
        return 0;
    }
}
