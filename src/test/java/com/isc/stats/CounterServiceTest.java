package com.isc.stats;

import com.isc.stats.configuration.TestConfiguration;
import com.isc.stats.dto.UploadEventDTO;
import com.isc.stats.service.CounterService;
import com.isc.stats.service.CounterUpdateScheduler;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestConfiguration.class)
public class CounterServiceTest {
    @Autowired
    private CounterService counterService;

    @Autowired
    private CounterUpdateScheduler updateScheduler;

    @Autowired
    @Qualifier("currentTime")
    private Long currentTime;


    @Test
    public void testConcurrentWrites() throws Exception {
        updateScheduler.stop();

        final int THREAD_COUNT = 1000;
        final int ITERATION_COUNT = 1000;
        List<Thread> threads = new ArrayList<>();

        CountDownLatch startLatch = new CountDownLatch(1);

        for (int i = 0; i < THREAD_COUNT; i++) {
            Thread thread = new Thread(() -> {
                try {
                    startLatch.await();
                } catch (InterruptedException ignored) {}

                for(int j = 0; j < ITERATION_COUNT; j++) {
                    counterService.registerEvent(new UploadEventDTO(5L, currentTime - 30_000L));
                }
            });

            threads.add(thread);
            thread.start();
        }

        startLatch.countDown();
        for (Thread thread : threads) {
            thread.join();
        }

        updateScheduler.flushQueue();

        Assert.assertEquals(5.0, counterService.getReport().getAverage(), 0.01);
        Assert.assertEquals(THREAD_COUNT * ITERATION_COUNT, counterService.getReport().getCount());
        Assert.assertEquals(THREAD_COUNT * ITERATION_COUNT * 5, counterService.getReport().getSum());
        Assert.assertEquals(5, counterService.getReport().getMax());
        Assert.assertEquals(5, counterService.getReport().getMin());
    }
}
