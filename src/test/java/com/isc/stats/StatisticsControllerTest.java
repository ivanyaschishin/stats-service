package com.isc.stats;

import com.isc.stats.configuration.TestConfiguration;
import com.isc.stats.dto.UploadEventDTO;
import com.isc.stats.dto.UploadStatisticsDTO;
import com.isc.stats.service.CounterService;
import com.isc.stats.service.CounterUpdateScheduler;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TestConfiguration.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class StatisticsControllerTest {

    @LocalServerPort
    private int port;

    @Autowired
    @Qualifier("currentTime")
    private Long currentTime;

    @Autowired
    private TestRestTemplate restTemplate;

    @Autowired
    private CounterService counterService;

    @Autowired
    private CounterUpdateScheduler updateScheduler;

    @Before
    public void setUp() throws Exception {
        updateScheduler.stop();
    }

    @Test
    public void testFullFlow() {
        UploadStatisticsDTO statistics;
        ResponseEntity<String> response;
        UploadEventDTO event;

        // testing initial state
        statistics = restTemplate.getForObject(getUrl("/statistics"), UploadStatisticsDTO.class);

        Assert.assertEquals(0.0, statistics.getAverage(), 0.01);
        Assert.assertEquals(0, statistics.getCount());
        Assert.assertEquals(0, statistics.getMin());
        Assert.assertEquals(0, statistics.getMax());
        Assert.assertEquals(0, statistics.getSum());

        // check that endpoint processes non-obsolete events
        event = new UploadEventDTO(3, currentTime - 30_000L);
        response = restTemplate.postForEntity(getUrl("/upload"), event, String.class);

        Assert.assertEquals(HttpStatus.CREATED, response.getStatusCode());
        Assert.assertNull(response.getBody());

        updateScheduler.flushQueue(); // flush update execution queue

        // check statistics change after first event
        statistics = restTemplate.getForObject(getUrl("/statistics"), UploadStatisticsDTO.class);

        Assert.assertEquals(3.0, statistics.getAverage(), 0.01);
        Assert.assertEquals(1, statistics.getCount());
        Assert.assertEquals(3, statistics.getMin());
        Assert.assertEquals(3, statistics.getMax());
        Assert.assertEquals(3, statistics.getSum());

        // check that it rejects obsolete events
        event.setTimestamp(0L);
        response = restTemplate.postForEntity(getUrl("/upload"), event, String.class);

        Assert.assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());

        // reset and do full flow with mixed in obsolete and non-obsolete events
        counterService.resetCounters();

        updateScheduler.flushQueue(); // flush update execution queue

        statistics = restTemplate.getForObject(getUrl("/statistics"), UploadStatisticsDTO.class);

        Assert.assertEquals(0.0, statistics.getAverage(), 0.01);
        Assert.assertEquals(0, statistics.getCount());
        Assert.assertEquals(0, statistics.getMin());
        Assert.assertEquals(0, statistics.getMax());
        Assert.assertEquals(0, statistics.getSum());

        event = new UploadEventDTO(5, currentTime - 10_000L);
        restTemplate.postForLocation(getUrl("/upload"), event);

        event = new UploadEventDTO(2, currentTime - 10_000L);
        restTemplate.postForLocation(getUrl("/upload"), event);

        event = new UploadEventDTO(7, currentTime - 10_000L);
        restTemplate.postForLocation(getUrl("/upload"), event);

        event = new UploadEventDTO(3, currentTime - 10_000L);
        restTemplate.postForLocation(getUrl("/upload"), event);

        // obsolete events
        event = new UploadEventDTO(1, currentTime - 61_000L);
        restTemplate.postForLocation(getUrl("/upload"), event);

        event = new UploadEventDTO(8, currentTime - 61_000L);
        restTemplate.postForLocation(getUrl("/upload"), event);

        // 5 2 7 3 are not obsolete, 1 and 8 are.

        updateScheduler.flushQueue(); // flush update execution queue

        statistics = restTemplate.getForObject(getUrl("/statistics"), UploadStatisticsDTO.class);

        Assert.assertEquals((5 + 2 + 7 + 3) / 4.0, statistics.getAverage(), 0.01);
        Assert.assertEquals(4, statistics.getCount());
        Assert.assertEquals(2, statistics.getMin());
        Assert.assertEquals(7, statistics.getMax());
        Assert.assertEquals(5 + 2 + 7 + 3, statistics.getSum());
    }

    private String getUrl(String path) {
        return "http://localhost:" + port + path;
    }
}
