package com.isc.stats.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;

import java.time.Clock;
import java.time.Instant;
import java.time.ZoneOffset;

@Configuration
@Import(ApplicationConfiguration.class)
public class TestConfiguration {
    private final Long CURRENT_TIME = 600000L;

    @Bean
    @Primary
    public Clock testClock() {
        return Clock.fixed(Instant.ofEpochMilli(CURRENT_TIME), ZoneOffset.UTC);
    }

    @Bean
    public Long currentTime() {
        return CURRENT_TIME;
    }
}
