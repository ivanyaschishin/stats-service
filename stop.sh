#!/usr/bin/env bash

# typically docker runs under root user, although it is possible to run it under
# regular user, this is a necessary evil
sudo docker-compose down